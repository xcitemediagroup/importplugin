<?php
    function my_plugin_feed() {
    if ( !current_user_can( 'manage_options' ) )  {
    wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
    }
        //include "upload.php";
        if(isset($_FILES['my_upload']['name']) && count($_FILES['my_upload']['name']) == 2) {

            $mydb = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
            $mydb->autocommit(false);
            $mydb->begin_transaction();

            $import = new import();
            $result = $import->init($mydb,$_FILES);
            //TODO RESULT TESTER
            if($result[0] == false){
                echo '</br>';
                echo 'import failed' ;
                echo '</br>';
                if(isset($result[1])){
                    echo '</br>';
                    echo 'Error may be related to field(s): ' . $result[1] ;
                    echo '</br>';
                }
                if(isset($result[2])){
                    echo '</br>';
                    echo 'Error may be related to field(s): ' . $result[2] ;
                    echo '</br>';
                }
                $mydb->rollback();
            }else{

                echo '</br>';
                echo 'import success' ;
                echo '</br>';

                $mydb->commit();
                $mydb->autocommit(true);
                //TODO TEST 1 REmove post
                $clean = $import->cleanupwp($mydb);
                echo $clean;
                echo '</br>';

            }


            $mydb->close();
            echo '</br>';
            echo '<form action="" method="post">';
            echo '<input type="submit" value="Import More Files" name="submit">';
            echo '</br>';
            echo '<div hidden="hidden">';
            echo 'Function testing';
            echo '</br>';
            echo print_r($result);
            echo '</br>';
            echo '</div>';
            //echo print_r($result[1]);
            echo '</br>';


        } elseif(isset($_FILES['my_upload']['name']) && count($_FILES['my_upload']['name']) > 0 && count($_FILES['my_upload']['name']) != 2){
            echo '<form action="" method="post" enctype="multipart/form-data">';
            echo '<p>You must import 2 files!</p>';
            echo '<p>Select files to import:</p>';
            echo '<input type="file" name="my_upload[]" id="fileToUpload" multiple="multiple">';
            echo '</br>';
            echo '<input type="submit" value="Import Files" name="submit" class="submitme">';
            echo '</form>';
        } else {
            echo '<form action="" method="post" enctype="multipart/form-data">';
            echo '<p>Select files to import:</p>';
            echo '<input type="file" name="my_upload[]" id="fileToUpload" multiple="multiple">';
            echo '</br>';
            echo '<input type="submit" value="Import Files" name="submit" class="submitme">';
            echo '</form>';
        }
    }
//TODO ADD GLOBAL DESCRIPTOR OR PURGE Description pre upload
//TODO IN production NULL description in transaction on change append all the lines
//TODO first run TESTING append always Leave place for NULL replace

/**
 * Class import
 * Passes the Db Object around by reference
 * Only call Init from out side of the class all other methods are called from within
 */
class import {
    /**
     * Default values
     */
    private $__visibility = "visible";
    private $__manage_stock = "yes";
    private $_post_status = "pending";
    private $_post_type = "product";
    private $_taxonomy = "product_cat";
    private $_tag_taxonomy = "product_tag";
    //private $_prefix_posttitle = "Hanger #";
    private $_prefix_postnasme = "derailleur-hanger-";
    private $_postauthor = 1;
    public $modetermid = '';
    //This will be an array of Item descriptions to be loaded into existing items
    public $descriptiondump = [];
    // POST NAME AND AUTHOR

    /**
     * @param $db
     * @param $files
     * @return array
     */
    function init(&$db,$files){
        /**
         * Pre Save data massage goes here
         * Example, scrub X out Change Y Etc
         */
        $total = count($files['my_upload']['name']);
        // SET THE FILE VALIDATION CHECK TO DEFAULT FALSE
        $hangfile = false;
        $manfile = false;
        $result = false;
        $testresults = [];
        ini_set ( 'max_execution_time', 0);
        // Here we are checking for a hanger file and manfac file if the are not present the import fails
        for($i=0; $i<$total; $i++) {
            $filename = $files['my_upload']['name'][$i];
            if(strpos($filename,'hanger') !== false){
                $hangfile = $i;
            }
            if(strpos($filename,'manufacturer') !== false){
                $manfile = $i;
            }
        }
        //Return if we do not have the proper files
        if($hangfile === false || $manfile === false){
            return [false,'You need to select a manufacturer file and a hanger file'];
        }
        echo 'loading';
        for($i=0; $i<$total; $i++) {
            //We need to load the hangar file first other wise posts will not be created, also this sets what type of file we are working with
            if($i == 0){
                $j = $hangfile;
                $type = 'hang';
            } else {
                $j = $manfile;
                $type = 'man';
            }
            //echo $type;

            $result = false; // Results will control if the SQl get committed False = rollback True = commit
            $linkfile = $files['my_upload']['tmp_name'][$j];

            $file = file($linkfile);
            $colnames = str_getcsv($file[0], ",", "\""); // get Column names Text delimiter " and separator ,
            if(isset($key)) {
                unset($key);
            }
            // 10k is the max number of records you can import at once
            if(count($file) > 501){
                return [false,'500 records is the max you can upload at one time with out harming the server performance, please divide you files up into less then 500 per file, keep in mind empty files are ok'];
            }
            foreach ($file as $key => $item) { // Check each line of the File imported
                if(count($file) < 2) {
                    echo $type;
                    echo '</br>';
                    echo '</br>';
                    echo 'Import Failed One of the files is corrupted or locked by a program on your system, also you may have not set your delimiter to double quote or separator to comma ' . $files['my_upload']['name'][$j] ;
                    echo '</br>';
                    echo '</br>';
                    return [false, 'File read issue'];
                }
                if (count($file) < $key + 2) { // end the loop at the last key not counting the first
                    break;//break is bad umm K refactor this some day
                }

                $lineitemdata = str_getcsv($file[$key + 1], ",", "\""); // Text delimiter " and separator ,

                //CHECK FILES VIA IF AND CALL CHECK  FUNCTION
                if($type == 'hang') {
                    $result = $this->checkhang($db, $lineitemdata, $colnames);//Kicks off adding products and meta Moved description to custom field CALLED details
                } else {

                    $result = $this->checkman($db, $lineitemdata, $colnames);//Kicks off adding cats and TODO tags and description
                    //TODO make sure we add </br> to each line of data
                }

                if ($result[0] === false || $result == false) {// One line fails Roll it all back
                    echo '</br>';
                    echo 'Import Failed ' . print_r($result[1],true);
                    echo '</br>';
                    return array($result[0], $lineitemdata,$result);
                }

                // ADd up all results to what ever
                $testresults[] = $result;


            }
        }

        $dresult  = $this->add_descriptions($db);

        if($dresult[0] == false){
            echo '</br>';
            echo 'Import Failed, Description import failed' ;
            echo '</br>';
            return array($result[0], $lineitemdata,$result);
        }
        $testresults[] = $dresult; //likely not needed but just in case
        // Return success
        return array(true, $testresults);
    }

    function checkhang(&$db,$lineitemdata,$colnames){
        $post_results = [];

        $selector = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

        $sku = $lineitemdata[0];
        $posttitle = $lineitemdata[1];
        $postcontent = $lineitemdata[22];

        $postname = 'derailleur-hanger-' . $sku;

        //Set query results to empty
        $query_results = [];

        //CREATE POST EXISTS SQL QUERY CHECK
        //"SELECT `post_id` FROM `wp_postmeta` WHERE `meta_key` = \'_sku\' AND `meta_value` = 27"
        $sql_post_exists = "SELECT `post_id` FROM `wp_postmeta` WHERE `meta_key` = \"_sku\" AND `meta_value` = \"$sku\"";

        //EXECUTE POST EXISTS
        $post_query = $selector->query($sql_post_exists);
        $selector->close();

        //IF POST DOES NOT EXIST CREATE AND CHECK FOR SUCCESS IF DOES EXIST SET POST ID FOR PASSAGE
        if ($post_query === false || mysqli_num_rows($post_query) == 0) {
            // NO post exist create this shit
            $post_results = $this->add_post($db,$postname,$posttitle,$postcontent);
            if($post_results[0] === false){
                return array(false,$post_results);
            }else{
                $postid = $post_results[1];
            }
            //ADD POST META DATA ALWAYS OVERWRITE EXCEPT STOCK
            $postmeta_results = $this->add_postmeta($db,$lineitemdata,$postid);
            if($postmeta_results[0] === false){
                return array(false,$postmeta_results[1]);
            }

        }elseif($post_query !== false){
            // FOUND STUFF LET UPDATE THE SHIT OUT OF IT
            //DO THIS VIA FETCHER REFER LINE 150 - 202 POSTS CAT 204 271
            //$fetcher = mysqli_fetch_all($post_query);

            //$postid = $fetcher[0][0];
            $results_array = [];
            while ($row = $post_query->fetch_assoc()) {
                $results_array[] = $row;
            }
            //return ['post id is when exists' , $results_array[0]['post_id']];
            $postid = $results_array[0]['post_id'];
            $post_results = $this->update_post($db,$lineitemdata,$colnames,$postid);

        }
        if(isset($postmeta_results)) {
            return [true, $post_results, $postmeta_results];
        }else{
            return [true, $post_results];
        }


    }



    function add_post(&$db,$postname,$posttitle,$postcontent){
        //ADD POST HERE

        $sql_add_post = "INSERT INTO `wp_posts`( `post_title`,`post_status`,`post_name`, `post_type`)\n"
            . "VALUES (\"$posttitle\",\"$this->_post_status\",\"$postname\",\"$this->_post_type\")";

        $rq = $db->query($sql_add_post);

        if($rq === false){

            return array (false, "Add post failed");

        }else{
            $id = $db->insert_id;

            return array(true, $id);
        }
    }

    function add_postmeta(&$db,$lineitemdata,$id){
        //ADD POST META


        $sku = $lineitemdata[0];
        $price = $lineitemdata[2];
        $weight = floatval($lineitemdata[3]) / 16;// Convert to lbs from oz For fedex fix
        $boltholes = $lineitemdata[4];
        $producttype = $lineitemdata[5];
        $stock = $lineitemdata[6];
        $typeofaluminum = $lineitemdata[7];
        $castvscnc = $lineitemdata[8];
        $finish = $lineitemdata[9];
        $includeshardware = $lineitemdata[10];
        $mountside = $lineitemdata[11];
        $screworbolt = $lineitemdata[12];
        $numberofsb = $lineitemdata[13];
        $sbtype = $lineitemdata[14];
        $sbsize = $lineitemdata[15];
        $sbthread = $lineitemdata[16];
        $dimofhanger = $lineitemdata[17];
        $axletype = $lineitemdata[18];
        $axlediameter = $lineitemdata[19];
        $manufacturerpartno = $lineitemdata[20];
        $hangermanufacturer = $lineitemdata[21];
        $details = $lineitemdata[22];
        $equivalent = $lineitemdata[23];

        if(!isset($stock) || empty($stock)){
            $stock = 0;
        }

        if(!isset($price) || empty($price)){
            $price = 0;
        }

        // THE QUERY FOR ADDING METAS IN HINSIGHT I COULD HAVE DONE THIS A MUCH EASIER WAY
        $sql_add_postmeta_array = array (
            "INSERT INTO `wp_postmeta`( `post_id`, `meta_key`, `meta_value`)\n"
            . " VALUES ($id,\"_sku\",\"$sku\")",
            "INSERT INTO `wp_postmeta`( `post_id`, `meta_key`, `meta_value`)\n"
            . " VALUES ($id,\"_visibility\",\"$this->__visibility\")",
            "INSERT INTO `wp_postmeta`( `post_id`, `meta_key`, `meta_value`)\n"
            . " VALUES ($id,\"_manage_stock\",\"$this->__manage_stock\")",
            "INSERT INTO `wp_postmeta`( `post_id`, `meta_key`, `meta_value`)\n"
            . " VALUES ($id,\"_stock\",$stock)",
            "INSERT INTO `wp_postmeta`( `post_id`, `meta_key`, `meta_value`)\n"
            . " VALUES ($id,\"_regular_price\",$price)",
            "INSERT INTO `wp_postmeta`( `post_id`, `meta_key`, `meta_value`)\n"
            . " VALUES ($id,\"_price\",$price)",
            "INSERT INTO `wp_postmeta`( `post_id`, `meta_key`, `meta_value`)\n"
            . " VALUES ($id,\"_weight\",\"$weight\")",
            "INSERT INTO `wp_postmeta`( `post_id`, `meta_key`, `meta_value`)\n"
            . " VALUES ($id,\"bolt_holes\",\"$boltholes\")",
            "INSERT INTO `wp_postmeta`( `post_id`, `meta_key`, `meta_value`)\n"
            . " VALUES ($id,\"product_type\",\"$producttype\")",
            "INSERT INTO `wp_postmeta`( `post_id`, `meta_key`, `meta_value`)\n"
            . " VALUES ($id,\"typeofaluminum\",\"$typeofaluminum\")",
            "INSERT INTO `wp_postmeta`( `post_id`, `meta_key`, `meta_value`)\n"
            . " VALUES ($id,\"castvscnc\",\"$castvscnc\")",
            "INSERT INTO `wp_postmeta`( `post_id`, `meta_key`, `meta_value`)\n"
            . " VALUES ($id,\"finish\",\"$finish\")",
            "INSERT INTO `wp_postmeta`( `post_id`, `meta_key`, `meta_value`)\n"
            . " VALUES ($id,\"includeshardware\",\"$includeshardware\")",
            "INSERT INTO `wp_postmeta`( `post_id`, `meta_key`, `meta_value`)\n"
            . " VALUES ($id,\"mountside\",\"$mountside\")",
            "INSERT INTO `wp_postmeta`( `post_id`, `meta_key`, `meta_value`)\n"
            . " VALUES ($id,\"screworbolt\",\"$screworbolt\")",
            "INSERT INTO `wp_postmeta`( `post_id`, `meta_key`, `meta_value`)\n"
            . " VALUES ($id,\"numberofsb\",\"$numberofsb\")",
            "INSERT INTO `wp_postmeta`( `post_id`, `meta_key`, `meta_value`)\n"
            . " VALUES ($id,\"sbtype\",\"$sbtype\")",
            "INSERT INTO `wp_postmeta`( `post_id`, `meta_key`, `meta_value`)\n"
            . " VALUES ($id,\"sbsize\",\"$sbsize\")",
            "INSERT INTO `wp_postmeta`( `post_id`, `meta_key`, `meta_value`)\n"
            . " VALUES ($id,\"sbthread\",\"$sbthread\")",
            "INSERT INTO `wp_postmeta`( `post_id`, `meta_key`, `meta_value`)\n"
            . " VALUES ($id,\"dimofhanger\",\"$dimofhanger\")",
            "INSERT INTO `wp_postmeta`( `post_id`, `meta_key`, `meta_value`)\n"
            . " VALUES ($id,\"axletype\",\"$axletype\")",
            "INSERT INTO `wp_postmeta`( `post_id`, `meta_key`, `meta_value`)\n"
            . " VALUES ($id,\"axlediameter\",\"$axlediameter\")",
            "INSERT INTO `wp_postmeta`( `post_id`, `meta_key`, `meta_value`)\n"
            . " VALUES ($id,\"manufacturerpartno\",\"$manufacturerpartno\")",
            "INSERT INTO `wp_postmeta`( `post_id`, `meta_key`, `meta_value`)\n"
            . " VALUES ($id,\"hangermanufacturer\",\"$hangermanufacturer\")",
            "INSERT INTO `wp_postmeta`( `post_id`, `meta_key`, `meta_value`)\n"
            . " VALUES ($id,\"details\",\"$details\")",
        );

        $sql_add_postmeta_array[] = "INSERT INTO `wp_postmeta`( `post_id`, `meta_key`, `meta_value`)\n"
            . " VALUES ($id,\"equivalent_to\",\"$equivalent\")";

        if($stock == 0){
            $sql_add_postmeta_array[] = "INSERT INTO `wp_postmeta`( `post_id`, `meta_key`, `meta_value`)\n"
                . " VALUES ($id,\"_stock_status\",\"outofstock\")";
        } else {
            $sql_add_postmeta_array[] = "INSERT INTO `wp_postmeta`( `post_id`, `meta_key`, `meta_value`)\n"
                . " VALUES ($id,\"_stock_status\",\"instock\")";
        }
        foreach($sql_add_postmeta_array as $query){
            $rq = $db->query($query);

            if($rq === false){

                return array(false, "post meta failed query" . $query);
            }
        }

        return array(true,"post meta success");
    }


    function update_post(&$db,$lineitemdata,$colnames,$id){
        //ADJUST STOCK LEVELS
        //update post meta
        $update = [];
        $ukey = '';
        //Prototypes
        //$sql_add_stock = "UPDATE `wp_postmeta` SET `meta_value`=$stocklevel WHERE `post_id` = $id AND `meta_key` = \"_stock\"";
        //$sql_update_content = "UPDATE `wp_posts` SET `post_content`=\"$lineitemdata[7]\" WHERE `ID` = $id";

        //query construction
        $meta_pre_query = "UPDATE `wp_postmeta` SET `meta_value`=\"";
        $meta_mid_query = "\" WHERE `post_id` = ";
        $meta_end1_query = " AND `meta_key` = \"";
        $meta_end2_query = "\"";
        //LOAD ARRAY COLNAME => VALUE
        // CRA CRA CASE LOADER I DUNNO IF THIS SAVES TIME OR NOT GAHHHHH
        foreach($lineitemdata as $key => $value){
            switch($colnames[$key]){
                case 'sku':
                    $ukey = "_sku";
                    break;
                case 'product name':
                    $ukey = "post_title";
                    break;
                case 'description':
                    $ukey = "details";//TODO REMOVE AFTER TEST 1
                    break;
                case 'price (USD)':
                    $ukey = "_regular_price";
                    break;
                case 'weight (oz)':
                    $ukey = "_weight";
                    break;
                case 'bolt holes':
                    $ukey = "bolt_holes";
                    break;
                case 'product type':
                    $ukey = "product_type";
                    break;
                case 'inventory quantity':
                    $ukey = "_stock";
                    break;
                default:
                    $ukey = $colnames[$key];



            }
            $update[$ukey] = $value;
        }
        // CREATE UPDATE ARRAY
        $update_querys = [];
        foreach($update as $key => $value){
            if($key != "_sku" && $key != "_stock"){
                //TODO REMOVE POST CONTENT UPDATE FROM HERE ADD TO new meta
                if($key != "post_title" && $key !="details"){//TODO ADJUST AFTER TEST 1 POST REMOVE
                    //set update query UPDATE ALL THE META
                    if($key == '_weight'){
                        $value = floatval($value) / 16;
                    }
//                    $meta_pre_query = "UPDATE `wp_postmeta` SET `meta_value`=\"";
//                    $meta_mid_query = "\" WHERE `post_id` = ";
//                    $meta_end1_query = " AND `meta_key` = \"";
//                    $meta_end2_query = "\"";
                    $sql_update_meta = $meta_pre_query . $value . $meta_mid_query . $id . $meta_end1_query . $key .$meta_end2_query;
                    //echo "</br>";
                    //echo $sql_update_meta;
                    //echo "</br>";
                    $update_querys[] = $sql_update_meta;
                }elseif($key == "post_title"){
                    //set update query UPDATE POST TITLE DIRECTLY
                    $sql_update_post = "UPDATE `wp_posts` SET `post_title`=\"$value\" WHERE `ID` = $id";
                    $update_querys[] = $sql_update_post;
                }elseif($key == "details"){
                    //TODO REMOVE AFTER TEST 1
                    //set update query UPDATE POST CONTENT DIRECTLY(description)
                    //$sql_update_post = "UPDATE `wp_posts` SET `post_content`=\"$value\" WHERE `ID` = $id";//TODO this can not happen
                    //$update_querys[] = $sql_update_post;
                }

            }elseif($key == "_stock"){
                //add stock update here add the value no negative results set negative results
                $sql_get_current_stock = "SELECT `meta_value` FROM `wp_postmeta` WHERE `meta_key` = \"_stock\" AND `post_id` = $id";
                $get_stock_query = $db->query($sql_get_current_stock);

                if($get_stock_query === false){
                    echo $id . "\n";
                    echo $sql_get_current_stock . "\n";
                    return array(false,"get stock amount failed");
                }

                $results_array = [];
                while ($row = $get_stock_query->fetch_assoc()) {
                    $results_array[] = $row;
                }
                $stocklevel = $results_array[0]['meta_value'];
                $stocklevel = intval($stocklevel) + intval($value);

                //Stock can not go below zero
                if($stocklevel < 0 || empty($stocklevel)){
                    $stocklevel = 0;
                    /*
                    echo '</br>';
                    echo 'You tired to subtract more stock then you have the item stock is now zero product name is: ' . $update['post_title'];
                    echo '</br>';
                    */
                }

                $sql_add_stock = "UPDATE `wp_postmeta` SET `meta_value`=$stocklevel WHERE `post_id` = $id AND `meta_key` = \"_stock\"";
                $add_stock_results = $db->query($sql_add_stock);

                if($add_stock_results === false){
                    return array(false,"stock update failed");
                }
            }

        }
        //EXCUTE ALL THE QUERIES
        foreach($update_querys as $query){
            $rq = $db->query($query);

            if($rq === false){
                return array(false, "update post and meta failed query" . $query);
            }
        }

        return [true,"update the post success"];
    }

    function checkman(&$db,$lineitemdata,$colnames){
        //MANUFACT IMPORT STUFF HERE
        //echo '</br>';
        //echo print_r($lineitemdata);
        //echo '</br>';


        //this will keep the could sites server alive .... It does and it is horrible looking
        //With out this the server will time out
        echo '.';
	    flush();
    	ob_flush();

        $total = count($lineitemdata);
        $manname = $lineitemdata[0];
        $manname = trim($manname," \t\n\r\0\x0B");
        $manname = trim($manname,".");
        $manname = trim($manname,"&");
        $sku = $lineitemdata[1];
        $maincatid = 359;
        //Check for post id if does not exist fail
        $sql_post_exists = "SELECT `post_id` FROM `wp_postmeta` WHERE `meta_key` = \"_sku\" AND `meta_value` = \"$sku\"";
        $post_query = $db->query($sql_post_exists);
        if ($post_query === false || mysqli_num_rows($post_query) == 0) {
            return [false, 'NO product exists with sku ' . $sku];
        }
        while ($prow = $post_query->fetch_assoc()) {
            $postid = $prow['post_id'];
        }

        //Checkif item is part of main cat and add if not
        $result = $this->check_item_cat($db,$postid,$maincatid);
        if($result[0] == false){
            return [false,"adding parent cat failed post id : $postid" => $result];
        }

        $results = $this->check_man_cat($db,$manname,$sku,$postid,$maincatid);//THIs can only trigger on maunfact is!
        if($results[0] == false){
            return [false,"checking main cat failed post id : $postid name : $manname mainid : $maincatid" , $results];
        }

        //TEST 1 TODO STUFF
        for($i =3;$i<$total;$i++) {
            //INIT the model LOOP here
            if($i % 2 != 0 && !empty($lineitemdata[$i])) {
                //TODO MODEL TAG STUFFS
                $modelname = $lineitemdata[$i];
                $modelname = trim($modelname, " \t\n\r\0\x0B\x20");
                $modelname = trim($modelname,".");
                $modelname = trim($modelname,"&");
                $type = 'model';
                $results = $this->check_sub_cat($db,$modelname,$postid);

            }elseif($i == 4 || (!empty($lineitemdata[$i]) && !empty($lineitemdata[($i-1)]))){
                //TODO YEAR STUFF SET THE ARRAY FOR ALL CONTENT
                $m = $i-1;//Model name

                $content = "<div class=\\\"$lineitemdata[0]\\\">". $lineitemdata[0] . ": <b>" . $lineitemdata[$i-1] . "</b>: " . $lineitemdata[$i] . "</div>";
                $this->load_desc($db,$content,$lineitemdata[1]);
            }elseif($i != 4 && !empty($lineitemdata[($i-1)])){
                $content = "<div class=\\\"$lineitemdata[0]\\\">". $lineitemdata[0] . ": <b>" . $lineitemdata[$i-1] . "</b></div>";
                $this->load_desc($db,$content,$lineitemdata[1]);
            }

        }

        return [true,'check man result placeholder' => $results];
    }

    function load_desc(&$db,$content,$sku){

        //FIND THE ID BY SKU
        $sql_post_exists = "SELECT `post_id` FROM `wp_postmeta` WHERE `meta_key` = \"_sku\" AND `meta_value` = \"$sku\"";

        //EXECUTE POST EXISTS
        $post_query = $db->query($sql_post_exists);

        //IF POST DOES NOT EXIST THIS IS AN ISSUE THAT SHOULD NEVER HAPPEN
        if ($post_query === false || mysqli_num_rows($post_query) == 0) {
            echo "Should never happen, no post id was found for sku: " . $sku;
            return [false, "finding the post id failed"];
        }else{
            //GIMME POSTID
            while ($prow = $post_query->fetch_assoc()) {
                $postid = $prow['post_id'];
            }
        }

        //TODO LOAD THE ARRAY
        if(isset($postid)) {//Double check that we have the post id
            $this->descriptiondump[$postid] .= $content;
        }else{
            echo "Should never happen, post id was found for sku: " . $sku . " however postid is not found";
            return [false, "finding the post id worked but we don't have it "];
        }

        return [true,"description array wad loaded"];
    }

    function add_descriptions(&$db){
        //echo print_r($this->descriptiondump,true);

        //TODO add all the descriptions to the DB
        //$sql_update_post = "UPDATE `wp_posts` SET `post_content`=\"$value\" WHERE `ID` = $id";

        //TODO GET THE OLD DESCRIPTION and apped it remove append at the end
        foreach($this->descriptiondump as $key => $content){
            //GET THE OLD POST CONTENT TODO REMOVE AFTER IMPORT
            //GET THE OLD POST CONTENT TODO REMOVE AFTER IMPORT
            //GET THE OLD POST CONTENT TODO REMOVE AFTER IMPORT
            //GET THE OLD POST CONTENT TODO REMOVE AFTER IMPORT
            //GET THE OLD POST CONTENT TODO REMOVE AFTER IMPORT
            //GET THE OLD POST CONTENT TODO REMOVE AFTER IMPORT
            
            $importcontent = '';
            $oldcontent = '';
            $sql_post_content = "SELECT `post_content` FROM `wp_posts` WHERE `ID` = \"$key\"";
            $post_query = $db->query($sql_post_content);

            if ($post_query === false || mysqli_num_rows($post_query) == 0) {
                echo "Should never happen, no post id was found for the arrary context: " . print_r($this->descriptiondump[$key]);
                return [false, "finding the post id failed on add description"];
            }else{
                //GIMME OLD CONTENT
                while ($prow = $post_query->fetch_assoc()) {
                    $oldcontent = $prow['post_content'];
                }
            }
            //$importcontent = $oldcontent;
            //$importcontent = addslashes($importcontent);// Add the slahsesd back to the content
            //$importcontent = "test this no quotes";
            $importcontent = null;
            if(!empty($importcontent)) {
                $content = $importcontent . $content;
            }
            //$content = "test 1";
            //END TODO REMOVE GET OLD POST CONTENT
            //END TODO REMOVE GET OLD POST CONTENT
            //END TODO REMOVE GET OLD POST CONTENT
            //END TODO REMOVE GET OLD POST CONTENT
            //END TODO REMOVE GET OLD POST CONTENT
            //END TODO REMOVE GET OLD POST CONTENT
            //echo "</br>";
            //echo print_r($content,true);
            //echo "</br>";

            $sql_update_content = "UPDATE `wp_posts` SET `post_content`=\"$content\" WHERE `ID` = $key";
            $add_content_results = $db->query($sql_update_content);
            echo "</br>";
            echo print_r($sql_update_content,true);
            echo "</br>";
            if($add_content_results === false){
                return array(false,"content update failed");
            }
        }




        return [true,"added the description to word press worked"];
    }

    function add_man_cat(&$db,$mancat,$sku,$postid,$parentid = 359){
        //TODO MAKE SURE THIS ONLY TRIGGERS ON MANUFACTS it should only do that by defualt
        $slug =str_replace(" ","-",$mancat);
        $slug = str_replace('.', "-",$slug);
        $slug = str_replace('&', "-",$slug);
        $slug = str_replace('+', "-",$slug);
        $slug = str_replace(':', "-",$slug);
        $slug = str_replace('----', "-",$slug);
        $slug = str_replace('---', "-",$slug);
        $slug = str_replace('--', "-",$slug);
            //$modelname = str_replace('.',' point ',$modelname);
        //ADD MANUFACT CAT
        //Proto type "INSERT INTO `wp_postmeta`( `post_id`, `meta_key`, `meta_value`)\n" . " VALUES ($id,\"_sku\",\"$sku\")"
        $sql_add_man_cat = "INSERT INTO `wp_terms` (`name`,`slug`) VALUES(\"$mancat\",\"$slug\")";
        $rq = $db->query($sql_add_man_cat);

        if($rq === false){
            return array(false, "add cat failed " . $sql_add_man_cat);
        }else{
            $tid = $db->insert_id;
        }

        //ADD META PARENT DATA TO CAT
        $sql_add_meta_data = "INSERT INTO `wp_term_taxonomy` (`term_id`, `taxonomy`, `parent`) VALUES (\"$tid\", \"$this->_taxonomy\", \"$parentid\")";
        $rq = $db->query($sql_add_meta_data);

        if($rq === false){
            return array(false, "add cat failed " . $sql_add_man_cat);
        }

        //ADD ITEM TO CAT
        $result = $this->add_item_cat($db,$postid,$tid);
        if($result[0] == false){
            return[false,'adding item to cat failed',$result];
        }

        //echo '</br>';
        //echo 'we get here in a man cat create';
        //echo '</br>';

        return[true,'add man cat placeholder' , $result,$tid];
    }

    function check_man_cat(&$db,$mancat,$sku,$postid,$parentid = 359){
        //CHECK IF MANUFACT CAT EXIST
        //TODO this can only be for manufacts - should be the case
        //Garneau test cat that is above the threshold does not exist with parent
        //Airnimal existing with threshold and out of threshold
        //Set sentnail for error checking
        $result = [false];
        //$mancattester = ['Garneau' , 'Airnimal','TESATNONEXISTDJKDJJD'];


        //Testing for loop TODO REMOVE
        //foreach($mancattester as $mancat) {
            $sql_cat_exists = "SELECT `term_id` FROM `wp_terms` WHERE `name` = \"$mancat\"";

            //EXECUTE cat EXISTS
            $cat_query = $db->query($sql_cat_exists);

            if ($cat_query === false || mysqli_num_rows($cat_query) == 0) {
                //TODO NO CAT EXISTS CREATE IT AND ADD THE ITEM TO IT
                $result = $this->add_man_cat($db,$mancat,$sku,$postid);
                if($result[0] == false){
                    echo '</br>';
                    echo 'cat add failed';
                    echo '</br>';
                    return [false,'add cat failed or ad item to cat failed in any case nothing has been imported'];
                }
                $termid = $result[3];
            } elseif ($cat_query !== false) {
                //SET ASSOIYED TO FALSE ADD CAT IF THIS STAYS FALSE
                $assoited = [false];
                //WE FOUND SOMETHING NOT DONE CHECK TO SEE IF IT IS PART OF THE MAIN CAT

                //NEW FOUND SOMETHING BUT IT MIGHT NOT BE A CAT IT COUNLD BE A TAG :( Fixed with aterm

                while ($row = $cat_query->fetch_assoc()) {
                    //set the term id
                    $atermid = $row['term_id'];
                    //echo '</br>';
                    //echo 'pre assioted';
                    //echo '</br>';
                    //echo print_r($row,true);
                    //echo '</br>';
                    $sql_cat_assoited = "SELECT `term_id`,`taxonomy` FROM `wp_term_taxonomy` WHERE `parent` = \"$parentid\" AND `term_id` = \"$atermid\"";
                    $assoited_query = $db->query($sql_cat_assoited);
                    //CHECK FOR ASSOATION
                    if ($assoited_query === false || mysqli_num_rows($assoited_query) == 0) {
                        // DO nothing not found
                    } elseif ($assoited_query !== false) {
                        //FOUND MATCH set assoited to true set termid
                        while ($arow = $assoited_query->fetch_assoc()) {
                            //set $as array and termid
                            $termid = $arow['term_id'];
                            $assoited = [true,$termid];
                        }
                    }
                }
                if($assoited[0] == true){
                    //CHECK IF ITEM IS PART OF CAT  IF not ADD ITEM
                    $result = $this->check_item_cat($db,$postid,$termid);
                    if($result[0] == false){
                        return[false,$result];
                    }
                }elseif($assoited[0] == false){
                    //TODO CAT DOES NOT EXIST INSDIE OF THE MAIN PARENT CREATE IT ND ADD item
                    $result = $this->add_man_cat($db,$mancat,$sku,$postid);
                    // check sub cat ?

                    if($result[0] == false){
                        return [false,$result];
                    }
                    //echo '</br>';
                    //echo 'we get here after add man cat dc';
                    //echo '</br>';
                }
            }
        //}// End test for loop


        return[true,'Check cat placeholder',$result,$termid];

    }

    function is_sub_cat_of_parent(){
        //Stub not used
    }


    function add_tag(&$db,$cat,$sku,$postid){
        //Adding tags here
        $parentid = 0;

        //SCrub the data so WP does not freak out
        $slug =str_replace(" ","-",$cat);
        $slug = str_replace('.', "-",$slug);
        $slug = str_replace('&', "-",$slug);
        $slug = str_replace('+', "-",$slug);
        $slug = str_replace(':', "-",$slug);
        $slug = str_replace('----', "-",$slug);
        $slug = str_replace('---', "-",$slug);
        $slug = str_replace('--', "-",$slug);
        //Make sure we are not importing nothing
        if(!empty($slug)) {

            //Query to add the tag to terms
            $sql_add_tag = "INSERT INTO `wp_terms` (`name`,`slug`) VALUES(\"$cat\",\"$slug\")";
            $rq = $db->query($sql_add_tag);

            if ($rq === false) {
                return array(false, "add cat failed " . $sql_add_tag);
            } else {
                $tid = $db->insert_id;
            }

            //ADD META DATA TO TERM TO MAKE IT A TAG
            $sql_add_meta_data = "INSERT INTO `wp_term_taxonomy` (`term_id`, `taxonomy`, `parent`) VALUES (\"$tid\", \"$this->_tag_taxonomy\", \"$parentid\")";
            $rq = $db->query($sql_add_meta_data);

            if ($rq === false) {
                return array(false, "add cat failed " . $sql_add_tag);
            }

            //ADD ITEM TO TAG
            $result = $this->add_item_cat($db, $postid, $tid);

            if ($result[0] == false) {
                return [false, 'adding item to cat failed', $result];
            }
        }


        return [true, 'add tag model placeholder'];
    }

    // THIS BEEN REPURPOSED FOR TAG IMPORT ONLY So parent should no longer be needed
    function check_sub_cat(&$db,$cat,$postid){
        //check if sub cat exists

        //NEW VARS
        // $CAT = TAG NAME
        // $cat_query = TAG TERM QUERY

        //TODO  make this call add_tag and ad dthe tag to the item if you add a tag
        $sku = 0;
        $sql_cat_exists = "SELECT `term_id` FROM `wp_terms` WHERE `name` = \"$cat\"";//TODO look for a tag
        $cat_query = $db->query($sql_cat_exists);//TODO this will become tag but the name will stay

        if ($cat_query === false || mysqli_num_rows($cat_query) == 0) {
            //NO CAT EXISTS CREATE IT AND ADD THE ITEM TO IT TODO TAG
            $result = $this->add_tag($db,$cat,$sku,$postid);//Call add tag then call add to item form that
            //echo '</br>';
            //echo 'shit trigger add sub cat worked parent :' . $parentid . " : " . $cat;
            //echo '</br>';
            if($result[0] == false){
                echo '</br>';
                echo 'tag add failed';
                echo '</br>';
                return [false,'add tag failed or ad item to tag failed in any case nothing has been imported'];
            }

            $termid = $result[3];

            //echo '</br>';
            //echo 'term in found false :' . $termid;
            //echo '</br>';
        } elseif ($cat_query !== false) {
            //TODO FULL REFACTOR when we find a tag we are done with making a tag we may need to add to an item still
            //WE found something but it may not be what we need
            //SET ASSOIYED TO FALSE ADD CAT IF THIS STAYS FALSE
            $assoited = [false];
            $parentid = 0;
            while ($row = $cat_query->fetch_assoc()) {//TODO REMOVE
                //set the term id
                $atermid = $row['term_id'];
                //echo '</br>';
                //echo 'pre assioted';
                //echo '</br>';
                //echo print_r($row,true);
                //echo '</br>';
                $sql_cat_assoited = "SELECT `term_id` FROM `wp_term_taxonomy` WHERE `parent` = \"$parentid\" AND `term_id` = \"$atermid\"";//Find the assoited tern if it exists
                $assoited_query = $db->query($sql_cat_assoited);
                //CHECK FOR ASSOATION
                if ($assoited_query === false || mysqli_num_rows($assoited_query) == 0) {
                    // DO nothing not found
                } elseif ($assoited_query !== false) {
                    //FOUND MATCH set assoited to true set termid
                    $counttest = 0;
                    while ($arow = $assoited_query->fetch_assoc()) {//Found a record that matches
                        //set $as array and termid
                        $termid = $arow['term_id'];
                        $counttest = $counttest +1;
                        //echo '</br>';
                        //echo 'assioted ' . $parentid ." " . $termid . "  count " . $counttest;
                        //echo '</br>';
                        //echo print_r($row,true);
                        //echo '</br>';
                        $assoited = [true,$termid];
                    }
                }
            }
            if($assoited[0] == true){
                //CHECK IF ITEM IS PART OF CAT  IF not ADD ITEM
                $result = $this->check_item_cat($db,$postid,$termid);//TODO ADd item check for tag
                if($result[0] == false){
                    echo '</br>';
                    echo 'check item failed :' . $termid;
                    echo '</br>';
                    return[false,$result];
                }
                //echo '</br>';
                //echo 'check item triggered ass true :' . $termid . " " . $parentid;
                //echo '</br>';
            }elseif($assoited[0] == false){
                //Tag does not exist as a tag and is just a cat name thus we need to add it
                $result = $this->add_tag($db,$cat,$sku,$postid);;
                //echo '</br>';
                //echo 'exp trigger add sub cat worked parent :' . $parentid . " : " . $cat;
                //echo '</br>';
                if($result[0] == false){
                    echo '</br>';
                    echo 'add model tag failed termid :' . $parentid . " " . $termid;
                    echo '</br>';
                    return [false,$result];
                }
                //echo print_r($result,true);
                $termid = $result[3];


            }

        }



        if(isset($termid)) {
            //RETURN THE TERMID FOR THE YEARS TO USE
            return [true, 'Check tag model placeholder',$termid];
        }else{//TODO SHOULD NEVER HAPPEN REMOVE
            return [true, 'Should never reach or the data in model is wrong'];
        }
    }

    function check_item_cat(&$db,$postid,$termid)//TODO THIS CAN ONLY TRIGGER on manufacts This is pulling with tag and cats sdhould be good
    {
        //CHECK IF ITEM IS PART OF EXISTING CAT ADD IF NOT ADD Item CAT
        $sql_check_item_cat = "SELECT `object_id` FROM `wp_term_relationships` WHERE `object_id` = \"$postid\" AND `term_taxonomy_id` = \"$termid\"";
        $assoited_query = $db->query($sql_check_item_cat);
        if ($assoited_query === false || mysqli_num_rows($assoited_query) == 0) {

            //ADd item to cat pre it is not part of it
            $aresult = $this->add_item_cat($db,$postid,$termid);
            if($aresult[0] == false){
                return [false,'add item cat failed',$aresult];
            }
        }elseif ($assoited_query !== false) {
            //echo 'item check found this :' . $postid . " "  . $termid;
        }
        if(isset($aresult)) {
            return [true, 'check item to cat success', $aresult];
        }else{
            return [true, 'check item to cat success'];
        }
    }
    function add_item_cat(&$db,$postid,$termid){//TODO THIS CAN ONLY TRIGGER ON MAUFACTS TEST 1 SHOULD ONLY MANUFACTS
        //ADD THE ITEM TO THE CAT IF THIS IS CALLED
        //wp_term_relationships
        $sql_add_item_to_cat = "INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`) VALUES (\"$postid\", \"$termid\")";
        $rq = $db->query($sql_add_item_to_cat);
        if($rq === false){
            return[false,'Adding the item to the cat failed'];
        }
        $rq = $this->new_add_count($db,$termid);
        if($rq === false){
            return[false,'Adding the item count failed'];
        }
        return[true,'add item to cat success'];
    }

    /**
     * @param $db
     * @param $tid
     * @return bool
     * This is the new count function to resolve the existing product issue
     */
    function new_add_count(&$db,$tid){//TODO this needs to increase the tag count add param to if trigger WILL TAKE TAGS TEST 1 will not do this on tags
        //$sql_add_meta_data = "INSERT INTO `wp_term_taxonomy` (`term_id`, `taxonomy`, `parent`) VALUES (\"$tid\", \"$this->_taxonomy\", \"$parentid\")";
        //UPDATED FOR SUPPORT OF TAGS wooo
        $sql_check_item_count = "SELECT `term_id`,`taxonomy`,`count` FROM `wp_term_taxonomy` WHERE `term_id` = \"$tid\" AND  (`taxonomy` = \"product_cat\" OR `taxonomy` = \"product_tag\")";
        $assoited_query = $db->query($sql_check_item_count);
        //TESTING COUNT FOR BAD COUNT CHECKS
        /*
        $sql_check_item_count_test = "SELECT `term_taxonomy_id` FROM `wp_term_relationships` WHERE `term_taxonomy_id` = \"$tid\"";
        $assoited_query_test = $db->query($sql_check_item_count_test);
        if($tid == 609905 || $tid ==609901) {
            echo "</br>";
            echo $tid . " count tester ";
            echo "</br>";
            echo mysqli_num_rows($assoited_query_test);
            echo "</br>";
        }*/

        if ($assoited_query === false || mysqli_num_rows($assoited_query) == 0) {
            //well this should not happen
            echo 'adding the count found nothing meaning this cat does not yet exist this should not be possible ';
            return false;
        }else{
            while ($arow = $assoited_query->fetch_assoc()) {
                $count = $arow['count'];
                $count = intval($count);
            }

            $count = $count + 1;
            $sql_update_count = "UPDATE `wp_term_taxonomy` SET `count`=\"$count\" WHERE `term_id` = \"$tid\" AND (`taxonomy` = \"product_cat\" OR `taxonomy` = \"product_tag\")";
            $rq = $db->query($sql_update_count);
            if($rq === false){
                echo '  update count failed    ';
                return false;
            }

        }

        return true;
    }

    /**
     * @param $db
     * @param $tid
     * @return bool
     * This is a fuckup function that I may need
     */
    function add_count(&$db,$tid){//TODO REMOVE NO LONGER USED
        //CHECK IF COUNT EXISTS
        $sql_check_item_count = "SELECT `term_id`,`meta_key`,`meta_value` FROM `wp_termmeta` WHERE `term_id` = \"$tid\" AND  `meta_key` = \"product_count_product_cat\"";
        $assoited_query = $db->query($sql_check_item_count);

        if ($assoited_query === false || mysqli_num_rows($assoited_query) == 0) {
            //IF NOT ADD COUNT
            $sql_add_item_to_count = "INSERT INTO `wp_termmeta` (`term_id`, `meta_key`,`meta_value`) VALUES (\"$tid\", \"product_count_product_cat\",\"1\")";
            $rq = $db->query($sql_add_item_to_count);
            if($rq === false){
                return false;
            }

        }else{
            //IF SO ADD 1 to count
            while ($arow = $assoited_query->fetch_assoc()) {
                $count = $arow['meta_value'];
                $count = intval($count);
            }
            $count = $count + 1;
            $sql_update_termmeta = "UPDATE `wp_termmeta` SET `meta_value`=\"$count\" WHERE `term_id` = \"$tid\" AND `meta_key` = \"product_count_product_cat\"";
            $rq = $db->query($sql_update_termmeta);
            if($rq === false){
                return false;
            }
        }
        return true;





    }
    function  cleanupwp(&$db){//Still needed for single parent update :(
        //product_cat_children
        //"DELETE FROM `1013836_xcitestage1`.`wp_options` WHERE `wp_options`.`option_id` = 4357"
        // Delete silly cat childern cthulu eat them yesssss
        //"DELETE FROM `wp_options` WHERE `option_name` = \'product_cat_children\'" from phpmyadmin derrrp

        //TODO remove hard link and call wp home url

        //Call curl to hit the web page so wp does the thing I need to clean up and then the problem is gone
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://www.xcitestage1.com.php56-9.dfw3-2.websitetestlink.com/");
        //curl_setopt($ch, CURLOPT_URL, "http://localhost/derailleurhanger.com/");
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
        curl_exec($ch);
        curl_close($ch);

        $sql_cthulu = "DELETE FROM `wp_options` WHERE `option_name` = \"product_cat_children\"";
        $request = $db->query($sql_cthulu);
        if($request === false){
            return 'wp clean up failed import worked';
        }else {
            return 'clean up worked';
        }
    }
}
?>
