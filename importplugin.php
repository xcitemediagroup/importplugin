<?php
/*
Plugin Name: Import pluigin
Plugin URI: http://derp.com
Description: a test plugin
Version: 0.2
Author: hanco
Author URI: http://mikehancoski.com
License: GPL2
*/

include "loadfiles.php";
//add_action("wp_head", "awepop_add_view");



add_action( 'admin_menu', 'my_plugin_menu' );

function my_plugin_menu() {
    $page_title = 'Import Test';
    $menu_title = 'Import data';
    $capability = 'manage_options';
    $menu_slug = 'Importslug'; // may need to match plugin name
    $function = 'my_plugin_feed';

    add_options_page( 'My Plugin Options', 'testplugin', 'manage_options', 'my-unique-identifier', 'my_plugin_options' );
    add_menu_page( $page_title, $menu_title, $capability, $menu_slug, $function);
}

function my_plugin_options() {
    if ( !current_user_can( 'manage_options' ) )  {
        wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
    }
    echo '<div class="wrap">';
    echo '<p>Here is where the form would go if I actually had options.</p>';
    echo '</div>';
}






?>